## E-COMMERCE API DOCUMENTATION

***TEST ACCOUNTS:***
- Regular User:
    - email: user@mail.com
    - password: 1234
- Admin User:
    - email: admin@mail.com
    - password: 1234

***ADDED FEATURES***

***User End***
- Register = Cannot create user if the email is already been used
- Cart = the showned Product name and price is based on the product id and retrieves the data from product modal so when the admin changes eh name and price of the price it will updat the datas in the Cart page
- Delete product = to delete specific product in the products array in the user modal
- check box = can only click when the products was availble and the quantity is not equal to 0
- arrow toggle = that increase/decrease the quantity of the product and it updates the quantity of product in the user modal
- Total amount = only products that was click check will be computed
               = for each product the Product price (from product modal) multiplied by the quantity (from user modal)
- Order Summary page = only products that was checked in cart will be shown in the Order Summary
- Cancel Order Button = user can cancel order when the status of the purchase is in pending status
- Unavailable products = even there was a product already in the user cart it will mark unavailable and it cannot be included to the list of products upon place order

***Admin***
- All User = Retrives all user
- Admin/Regular = sets the user as Admin/Regular
- All Orders = Retrive all orders
- Purchase status button = changing status of the order purchased from "pending" to "to ship" and to "delivered"
- Cancel Order Button = admin can cancel order when the status of the purchase is in pending status

***Backend***
***used option 1 and option2 modals***
- user modal = user structure and handles the products of user in the cart
- products modal = products structure and handles all the users that added product
- order modal = orders structure and handles authenticated products