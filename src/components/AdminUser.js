import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminUser({user, isAdmin, fetchData}) {

	const adminToggle = (userId) => {
		fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/${userId}/admin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'User has been set as Admin'
				})
				fetchData();

			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}


		const unadminToggle = (userId) => {
		fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/${userId}/unadmin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'User has been set as Regular'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}
 

	return(
		<>
			{isAdmin ?

				<Button variant="danger" size="sm" onClick={() => unadminToggle(user)}>Set as Regular</Button>

				:

				<Button variant="success" size="sm" onClick={() => adminToggle(user)}>Set as Admin</Button>

			}
		</>

		)
}