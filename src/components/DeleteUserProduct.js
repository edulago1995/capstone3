import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteUserProduct({product}) {
	const productId = product.productId;
	// console.log(productId);
  const deleteToggle = (productId) => {
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/${productId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => {
        if (!res.ok) {
          // Handle non-successful response status codes here
          throw new Error('Network response was not ok');
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
        if (data) {
          Swal.fire({
            title: 'Success',
            icon: 'success',
            text: 'Successfully remove product',
          }).then(() => {
            // Reload the window
            window.location.reload();
          });
        } else {
          Swal.fire({
            title: 'Something Went Wrong',
            icon: 'error', // 'error' instead of 'Error'
            text: 'Please Try again',
          });
        }
      })
      .catch((error) => {
        console.error('Error:', error);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred while deleting the product.',
        }).then(() => {
            // Reload the window
            window.location.reload();
          });;
      });
  }; 
	return(
		<>
				<Button variant="danger" size="sm" onClick={() => deleteToggle(product)}>Remove</Button>
		</>

		)
}