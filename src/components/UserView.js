import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);
  const [numProductsPerRow, setNumProductsPerRow] = useState(4); // Default to 4 products per row

  useEffect(() => {
    // Define media query for extra-small and small screens
    const mediaQuery = window.matchMedia('(max-width: 576px)');

    // Function to update the number of products per row based on the screen size
    const updateNumProductsPerRow = () => {
      if (mediaQuery.matches) {
        setNumProductsPerRow(2); // 2 products per row for extra-small and small screens
      } else {
        setNumProductsPerRow(4); // 4 products per row for other screen sizes
      }
    };

    // Call the function initially and add a listener for screen size changes
    updateNumProductsPerRow();
    mediaQuery.addListener(updateNumProductsPerRow);

    // Clean up the listener when the component unmounts
    return () => {
      mediaQuery.removeListener(updateNumProductsPerRow);
    };
  }, []);

  useEffect(() => {
    const productChunks = chunkArray(productsData, numProductsPerRow);

    const productRows = productChunks.map((chunk, rowIndex) => (
      <div className="product-row" key={rowIndex}>
        {chunk.map((product) => (
          <div className={`product-card col-${12 / numProductsPerRow}`} key={product._id}>
            <ProductCard productProp={product} />
          </div>
        ))}
      </div>
    ));

    setProducts(productRows);
  }, [productsData, numProductsPerRow]);

  // Helper function to chunk the array into rows of 'chunkSize' products
  function chunkArray(arr, chunkSize) {
    const chunkedArray = [];
    for (let i = 0; i < arr.length; i += chunkSize) {
      chunkedArray.push(arr.slice(i, i + chunkSize));
    }
    return chunkedArray;
  }

  return (
    <>
      <ProductSearch />
      <div className="product-gallery">{products}</div>
    </>
  );
}