import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function ProductCard({productProp}) {

    const [products, setProducts] = useState([]);
    const { _id, name, description, price} = productProp;

    return (
        <Card className="mt-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price: PhP {price}</Card.Subtitle>
                <div className="text-center my-3">
            {productProp.isActive ? (
                <Link className="btn btn-primary" to={`/products/${_id}`}>
                  Details
                </Link>
            ) : (
                <button className="btn btn-primary" disabled>
                  (Not Available)
                </button>
            )}
            </div>
            </Card.Body>
        </Card>
        )
}


ProductCard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}