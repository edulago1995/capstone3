import React, { useState } from 'react';
import ProductCard from './ProductCard';
const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://cpstn2-ecommerceapi-lago.onrender.com/products/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for products:', error);
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === 'Enter' || event.key === 'Enter' || event.key === 'Enter') {
      handleSearch();
    }
  };


  return (
    <div className='pt-5 container'>
      <div className="form-group">
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
          onKeyPress={handleKeyPress}
          placeholder="Search for products..."
        />
      </div>
      <ul>
        {searchResults.map(product => (
          <ProductCard productProp={product} key={product._id}/>
        ))}
      </ul>
    </div>
  );
};

export default ProductSearch;