import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Link, useNavigate } from 'react-router-dom';
import { Table, Container, Col, Row, Button } from 'react-bootstrap';
import OrderStatus from './OrderStatus';
import CancelOrder from './CancelOrder';

export default function AllOrders({ ordersData, fetchData }) {
  const [users, setUsers] = useState([]);
  const { user } = useContext(UserContext);
  const navigate = useNavigate(); 

  useEffect(() => {
    // Check if usersData is defined and is an array before mapping over it
    if (Array.isArray(ordersData) && ordersData.length > 0) {
      const usersArr = ordersData.map((user) => (
        <tr key={user._id}>
          <td>{user._id}</td>
          <td className="text-center">{user.orderedProducts[0].products.map((productSet, productSetIndex) => (
              <div key={productSetIndex} className="product-set">
                <a id="total-amount">{productSet.name}</a>
              </div>
          ))}</td>
          <td className="text-center">{user.orderedProducts.map((productSet, productSetIndex) => (
              <div key={productSetIndex} className="product-set">
                <a id="total-amount">Php {productSet.totalAmount}</a>
              </div>
          ))}</td>
      		<td className={user.isActive && user.isPending ? 'text-success text-center' : (user.isActive && !user.isPending ? 'text-success text-center' : (!user.isActive && user.isPending ? 'text-danger text-center' : 'text-success text-center'))}>
      			 {user.isActive && user.isPending ? 'Pending' : (user.isActive && !user.isPending ? 'Shiped' : (!user.isActive && user.isPending ? 'Canceled' : 'Delivered'))}
      		</td>
          <td style={{ textAlign: 'center' }}>
            <OrderStatus user={user._id} isPending={user.isPending} isActive={user.isActive} fetchData={fetchData} />
          </td>
          <td style={{ textAlign: 'center' }}>
            <CancelOrder user={user._id} isPending={user.isPending} isActive={user.isActive} fetchData={fetchData} />
          </td>
        </tr>
      ));
	   console.log(ordersData)

      setUsers(usersArr);
    } else {
      // If usersData is undefined or an empty array, set users to an empty array
      setUsers([]);
    }
  }, [ordersData, fetchData]);
  console.log(user)
  

  return user.isAdmin === true ? (
    <>
      <Container>
        <Row>
          <Col>
            <h2 className="my-3 py-2">Admin Dashboard - ALL ORDERS</h2>
          </Col>
          <Col className="text-end my-4">
            <div>
              <Link to="/products">
                <Button variant="primary">All products</Button>
              </Link>
            </div>
          </Col>
        </Row>
      </Container>
      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>Order ID</th>
            <th>Product List</th>
            <th>Total Amount</th>
            <th>Status</th>
            <th>Actions</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{users}</tbody>
      </Table>
    </>
  ) : (
    (() => {
      navigate('/products');
      return null;
    })()
  );
}
