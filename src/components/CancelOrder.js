	import { Button } from 'react-bootstrap';
	import Swal from 'sweetalert2';

	export default function CancelOrder({user, isActive, isPending}) {

			const cancelToggle = (user) => {
			fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/${user}/deliver`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})

			.then(res => res.json())
			.then(data => {
				console.log(data)
				if (data === true) {
			  Swal.fire({
			    title: 'Success',
			    icon: 'success',
			    text: 'Order Canceled'
			  }).then(() => {
			    // Reload the page after displaying the success message
			    window.location.reload();
			  });
			} else {
			  Swal.fire({
			    title: 'Something Went Wrong',
			    icon: 'error', // Use 'error' for the error icon
			    text: 'Please Try again'
			  });
			}
			})
		}
	 	// console.log(user)
	 	// console.log(isActive)

		return (
		  <>
		    {isPending && isActive ? (
		      <Button variant="danger" size="sm" onClick={() => cancelToggle(user)}>
		        Cancel
		      </Button>
		    ) : (
		      <Button
		        variant="light"
		        size="sm"
		        onClick={() => cancelToggle(user)}
		        disabled={true}
		      >
		        Cancel
		      </Button>
		    )}
		  </>
		);


	}