import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function OrderStatus({user, isActive, isPending, fetchData}) {

	const toShipToggle = (userId) => {
		fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/${userId}/toship`, {
		    method: 'PUT',
		    headers: {
		        'Content-Type': 'application/json',
		        Authorization: `Bearer ${localStorage.getItem('token')}`
		    }
		})
		.then(res => {
		    if (!res.ok) {
		        throw new Error('Network response was not ok');
		    }
		    return res.json();
		})
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'products has been shipped'
				})
				fetchData();

			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}


		const deliverToggle = (userId) => {
		fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/${userId}/deliver`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product has been delivered'
				})
				fetchData();
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData();
			}


		})
	}
 

	return (
	  <>
	    {isPending && isActive ? (
	      <Button variant="danger" size="sm" onClick={() => toShipToggle(user)}>
	        To Ship
	      </Button>
	    ) : isActive && !isPending ? (
	      <Button variant="success" size="sm" onClick={() => deliverToggle(user)}>
	        Deliver
	      </Button>
	    ) : !isActive && isPending ? (
	      <p>-</p>
	    ) : (
	      <p>-</p>
	    )}
	  </>
	);
}