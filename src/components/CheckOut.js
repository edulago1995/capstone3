import '../App.css'; // Import the CSS file
import { Button } from 'react-bootstrap'; // Import Button from react-bootstrap (assuming you're using Bootstrap)
import Swal from "sweetalert2";
import { Navigate } from 'react-router-dom';

function CheckOut({ userDetails, orderedProduct, totalAmount }) {

  const handleCheckout = () => {
    const token = localStorage.getItem('token');
    
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/orderProduct`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(orderedProduct)
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      if (data) {
        Swal.fire({
          icon: "success",
          title: "Order Placed"
        }).then(() => {
      // Redirect to /MyPurchase after clicking "OK"
      window.location.href = '/MyPurchase';
    });
      } else {
        Swal.fire({
          icon: "error",
          title: "Unsuccessful Product Creation",
          text: data.message
        })
      }
    });
  }

  // to get data
  // console.log(userDetails[0].firstName)

  return (
    <Button variant="danger" size="sm" onClick={handleCheckout}>
      Check Out
    </Button>
  );
}

export default CheckOut;
