import React from 'react';
import '../App.css';

function CustomNumberInput({ quantity, setQuantity, productId }) {
  // const productId = product.productId
  console.log(productId);

  const handleIncrement = () => {
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/${productId}/increment`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      }
    })
      .then((response) => {
        if (response.ok) {
          // Increment the quantity in your component state.
          setQuantity(quantity + 1);
        } else {
          // Handle the error here if the request fails.
          console.error('Failed to increment quantity');
        }
      })
      .catch((error) => {
        // Handle any network errors here.
        console.error('Network error:', error);
      });
  };

  const handleDecrement = () => {
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/${productId}/decrement`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      }
    })
      .then((response) => {
        if (response.ok) {
          // Decrement the quantity in your component state.
          setQuantity(quantity - 1);
        } else {
          // Handle the error here if the request fails.
          console.error('Failed to decrement quantity');
        }
      })
      .catch((error) => {
        // Handle any network errors here.
        console.error('Network error:', error);
      });
  };

  return (
    <div className="custom-number-input">
      <button onClick={handleDecrement}>&#9666;</button>
      <input
        type="text"
        id="quantity"
        value={quantity}
        onChange={(e) => setQuantity(e.target.value)}
        min={1}
        readOnly 
      />
      <button onClick={handleIncrement}>&#9656;</button>
    </div>
  );
}

export default CustomNumberInput;
