import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct';
import Profile from './pages/Profile';
import ProductCart from './pages/ProductCart';
import Users from './pages/Users';
import UpdateProfile from './pages/UpdateProfile';
import ResetPassword from './pages/ResetPassword';
import ReceiptPage from './pages/ReceiptPage';
import MyPurchase from './pages/MyPurchase';
import Orders from './pages/Orders';
// import SearchByPrice from './components/SearchByPrice';
import './App.css';
import { UserProvider } from './UserContext';


function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

    const unsetUser = () => {

      localStorage.clear();

    };

    useEffect(() => {

    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          email: data.email
        });

      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

    }, []);

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Products />} />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />}/>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/cart" element={<ProductCart />} />
            <Route path="/users" element={<Users />} />
            <Route path="/reset-password" element={<ResetPassword />} />
            <Route path="/updateprofile" element={<UpdateProfile />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/receipt" element={<ReceiptPage />} />
            <Route path="/MyPurchase" element={<MyPurchase />} />
            <Route path="/allorders" element={<Orders />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;