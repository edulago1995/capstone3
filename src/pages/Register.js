import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
  const { user } = useContext(UserContext);

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [emailExists, setEmailExists] = useState(false);
  const [allEmails, setAllEmails] = useState([]);

  useEffect(() => {
    // Fetch all registered emails when the component mounts
    fetch('https://cpstn2-ecommerceapi-lago.onrender.com/users/all/emails') // Replace with the actual endpoint
      .then((res) => res.json())
      .then((data) => {
        setAllEmails(data);
      })
      .catch((error) => {
        console.error('Error fetching emails:', error);
      });
  }, []);

  useEffect(() => {
    // Check if all conditions for activation are met
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNo !== '' &&
      password !== '' &&
      confirmPassword !== '' &&
      password === confirmPassword &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }

    // Check if the email already exists
    const emailExistsInDatabase = allEmails.includes(email);
    setEmailExists(emailExistsInDatabase);
  }, [firstName, lastName, email, mobileNo, password, confirmPassword, allEmails]);

  function registerUser(e) {
    e.preventDefault();
    // Check if the email already exists
    if (emailExists) {
      return; // Don't proceed with registration
    }

    // Perform the registration
    fetch('https://cpstn2-ecommerceapi-lago.onrender.com/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          setFirstName('');
          setLastName('');
          setEmail('');
          setMobileNo('');
          setPassword('');
          setConfirmPassword('');

          Swal.fire({
            title: 'Success!!',
            icon: 'success',
            text: 'Thank you for registering!',
          }).then((result) => {
            if (result.isConfirmed) {
              // Redirect to /login
              window.location.href = "/login";
            }
          });
        } else {
          Swal.fire({
            title: 'SORRY!',
            icon: 'error',
            text: 'Please input all the fields',
          });
        }
      });
  }

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Container>
      <Row>
        <Col lg={6} xl={6} xxl={6} style={{ display: 'flex' }}>
          {/* Left side of the screen */}
          <div
            style={{
              // display: 'flex',
              // flexDirection: 'column',
              alignItems: 'center', // Center horizontally
              justifyContent: 'center', // Center vertically
              marginTop: '10%',
              paddingLeft: '15%'
            }}
          >
            <a href="https://imgbb.com/">
              <img
                src="https://i.ibb.co/44GQtY5/350527252-1110909973631682-1129322120803573381-n-removebg-preview-removebg-preview.png"
                alt="350527252-1110909973631682-1129322120803573381-n-removebg-preview-removebg-preview"
                border="0"
                className="d-none d-md-block"
              />
            </a>
          </div>
        </Col>
        <Col lg={6} xl={6} xxl={6}>
          {/* Right side of the screen */}
          <Form onSubmit={(e) => registerUser(e)}>
            <h1 className="my-3 text-center">Register</h1>
            <Form.Group>
              <Form.Label>First Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter First Name"
                required
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Last Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                required
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter Email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {emailExists && (
                <p className="text-danger">Email already used.</p>
              )}
            </Form.Group>
            <Form.Group>
              <Form.Label>Mobile No:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter 11 Digit No."
                required
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter Password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="pb-3">
              <Form.Label>Confirm Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm Password"
                required
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </Form.Group>
            {isActive ? (
              <Button variant="primary" type="submit">
                Submit
              </Button>
            ) : (
              <Button variant="danger" disabled>
                Submit
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
