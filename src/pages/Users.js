import { useEffect, useState, useContext } from 'react';
// import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

import AllUsers from '../components/AllUsers';

export default function Users() {

	const { user } = useContext(UserContext);

	const [users, setProdcuts] = useState([]);


		const fetchData = () => {
			fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/all`, {
			method: 'GEt',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
			    
			    console.log(data);

			    setProdcuts(data);

			});
		}

	    useEffect(() => {

			fetchData()

	    }, []);


	return(
		<>
            {
            	(user.isAdmin === true) ?
            		<AllUsers usersData={users} fetchData={fetchData} />

            		:

            		<AllUsers usersData={users} />

        	}
        </>
	)
}
