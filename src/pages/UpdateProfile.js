import {useState,useEffect, useContext} from 'react';
import {Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate,Navigate,Link } from 'react-router-dom';
// import ResetPassword from '../components/ResetPassword';	
import UpdateProfileForm from '../components/UpdateProfile';
export default function UpdateProfile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

    useEffect(()=>{

        fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (typeof data._id !== "undefined") {

				setDetails(data);

			}
        });

    },[])

	return (
		  (user.id === null) ?
		  <Navigate to="/products" />
		  :
		  <>
		    <Row className='pt-4 mt-4'>
				<Col>
					<UpdateProfileForm />
				</Col>
			</Row>
		  </>
	)

}