import { useState, useEffect, useContext } from 'react';
import { Table } from 'react-bootstrap'; // Import Table component
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import UpdateQuantity from '../components/UpdateQuantity';
import DeleteUserProduct from '../components/DeleteUserProduct';
import Checkout from './ReceiptPage';

export default function ProductCart(fetchData) {
  const { user } = useContext(UserContext);
  const navigate = useNavigate(); // Create a navigate function for navigation

  const [details, setDetails] = useState({});
  const [productDetails, setProductDetails] = useState({});
  const [selectAll, setSelectAll] = useState(false); // State to track global toggle
  const [isAnyProductSelected, setIsAnyProductSelected] = useState(false);


    useEffect(() => {
    // Fetch user details
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data._id !== 'undefined') {
          setDetails(data);
        }
      });

    // Fetch product details
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        // Assuming the response data is an array of product details
        setProductDetails(data);
      });
  }, []);

  // Function to update the quantity of a product
  const updateQuantity = (orderIndex, productIndex, newQuantity) => {
    const updatedDetails = { ...details };
    updatedDetails.orderedProducts[orderIndex].products[
      productIndex
    ].quantity = newQuantity;
    setDetails(updatedDetails);
  };

  // Function to toggle the checkbox for a product
  const toggleProductCheckbox = (orderIndex, productIndex) => {
  const updatedDetails = { ...details };
    updatedDetails.orderedProducts[orderIndex].products[
      productIndex
    ].isChecked = !updatedDetails.orderedProducts[orderIndex].products[
      productIndex
    ].isChecked;
    setDetails(updatedDetails);

    // Check if any product is selected
    const anyProductSelected = updatedDetails.orderedProducts.some((order) =>
      order.products.some((product) => product.isChecked)
    );
    setIsAnyProductSelected(anyProductSelected);
  };


  // Function to handle global toggle
  const toggleAllCheckboxes = () => {
  const updatedDetails = { ...details };
    updatedDetails.orderedProducts.forEach((order) => {
      order.products.forEach((product) => {
        product.isChecked = !selectAll;
      });
    });
    setDetails(updatedDetails);
    setSelectAll(!selectAll);

    // Check if any product is selected
    const anyProductSelected = updatedDetails.orderedProducts.some((order) =>
      order.products.some((product) => product.isChecked)
    );
    setIsAnyProductSelected(anyProductSelected);
  };


  const calculateTotalAmount = () => {
    let total = 0;
    details.orderedProducts.forEach((order) => {
      order.products.forEach((product) => {
        if (product.isChecked && getProductAvailability(product.productId)) { // Check if the product is checked and available
          const productDetail = productDetails.find(
            (detail) => detail._id === product.productId
          );
          if (productDetail) {
            total += productDetail.price * product.quantity;
          }
        }
      });
    });
    return total;
  };

  const getProductPrice = (productId) => {
    if (!productId || !Array.isArray(productDetails)) {
      return 0; // Return a default value for invalid productId or when productDetails is not an array
    }
    const productDetail = productDetails.find((detail) => detail._id === productId);
    return productDetail ? productDetail.price : 0;
  };

  const getProductName = (productId) => {
    if (!productId || !Array.isArray(productDetails)) {
      return null; // Return null for invalid productId or when productDetails is not an array
    }
    const productDetail = productDetails.find((detail) => detail._id === productId);
    return productDetail ? productDetail.name : null;
  };

  const getProductAvailability = (productId) => {
    if (!productId || !Array.isArray(productDetails)) {
      return null; // Return null for invalid productId or when productDetails is not an array
    }
    const productDetail = productDetails.find((detail) => detail._id === productId);
    return productDetail ? productDetail.isActive : false;
  };

  // const handleDeleteProduct = (orderIndex, productIndex) => {
  //   const updatedDetails = { ...details };
  //   const products = updatedDetails.orderedProducts[orderIndex].products;
  //   // Remove the product from the products array
  //   products.splice(productIndex, 1);
  //   // Update the state with the modified details
  //   setDetails(updatedDetails);
  // };

  const handleCheckout = () => {
  // Create an array to store selected products
  const selectedProducts = [];

  // Calculate the total amount
  let totalAmount = 0;

  details.orderedProducts.forEach((order) => {
    order.products.forEach((product) => {
      if (product.isChecked && getProductAvailability(product.productId)) {
        const productDetail = productDetails.find(
          (detail) => detail._id === product.productId
        );
        if (productDetail) {
          totalAmount += productDetail.price * product.quantity;

          // Add the selected product to the array
          selectedProducts.push({
            product_id: productDetail._id,
            name: productDetail.name,
            price: productDetail.price,
            quantity: product.quantity,
          });
        }
      }
    });
  });

  // Navigate to the receipt page and pass the data as location state
  navigate('/receipt', {
    state: { selectedProducts, totalAmount },
  });
  };

return user.id === null ? (
  <Navigate to="/products" />
) : (
  <>
    <div className="text-center">
      <h1 className="display-4">My Cart</h1>
    </div>
    <div className="text-center">
      <h4>Products</h4>
      {Array.isArray(details.orderedProducts) && details.orderedProducts.length > 0 ? (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>
                <input
                  type="checkbox"
                  checked={selectAll}
                  onChange={toggleAllCheckboxes} // Handle global toggle
                />
              </th>
              <th>Product</th>
              <th>Price</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
            {details.orderedProducts.map((order, orderIndex) => (
              order.products.map((product, productIndex) => (
                <tr key={productIndex}>
                  <td>
                    <input
                      type="checkbox"
                      checked={(product.isChecked && getProductAvailability(product.productId)) || false}
                      disabled={product.quantity === 0 || !getProductAvailability(product.productId)}
                      onChange={() =>
                        toggleProductCheckbox(orderIndex, productIndex)
                      }
                    />
                  </td>
                  <td>{getProductName(product.productId)}</td>
                  <td>Php {getProductPrice(product.productId)}</td>
                  <td style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    {getProductAvailability(product.productId) ? (
                      <UpdateQuantity
                        productId={product._id}
                        quantity={product.quantity}
                        setQuantity={(newQuantity) =>
                          updateQuantity(orderIndex, productIndex, newQuantity)
                        }
                      />
                    ) : (
                      <h6 className="mt-2">Not Available</h6>
                    )}
                  </td>
                  <td><DeleteUserProduct product={product._id} fetchData={fetchData} /></td>
                </tr>
              ))
            ))}
          </tbody>

           <tbody>
              {details.orderedProducts.map((order, orderIndex) => (
                <tr key={orderIndex}>
                  <td colSpan="2"></td>
                  <td>Total:</td>
                  <td>Php {calculateTotalAmount()}</td>
                  <td>
                    {/* Call handleCheckout when the checkout button is clicked */}
                    <button
                      onClick={handleCheckout}
                      className={`btn btn-success btn-sm ${isAnyProductSelected ? '' : 'disabled'}`}
                      disabled={!isAnyProductSelected}
                    >
                      Place Order
                    </button>

                  </td>
                </tr>
              ))}
            </tbody>
        </Table>
      ) : (
        <p>No ordered products available.</p>
      )}
    </div>
  </>
);

}
