import {useState,useEffect, useContext} from 'react';
import React from 'react';
import { useLocation } from 'react-router-dom';
import CheckOut from '../components/CheckOut';
import "../App.css";

function ReceiptPage() {
  const location = useLocation();
  const { selectedProducts, totalAmount } = location.state;

  // console.log(selectedProducts);
  const orderedProduct= JSON.stringify(selectedProducts);
  console.log(orderedProduct);



  const [details,setDetails] = useState({})
    // this is to test if you get the user detail
    // console.log(details);

    useEffect(()=>{

        fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (typeof data._id !== "undefined") {

        setDetails(data);

      }
        });

    },[])

    const userDetails=[{
    firstName: details.firstName,
    lastName: details.lastName,
    email: details.email,
    mobileNo: details.mobileNo,
  }]

  console.log(userDetails)

  return (
      <div className="receipt-card">
        <h1 id="receipt-header">Order Summary</h1>
        <div className="receipt-content">
          <table id="receipt-table">
            <thead>
              <tr>
                <th id="table-header-name">Product Name</th>
                <th id="table-header-price">Price</th>
                <th id="table-header-quantity">Quantity</th>
              </tr>
            </thead>
            <tbody>
              {selectedProducts.map((product, index) => (
                <tr key={index}>
                  <td>{product.name}</td>
                  <td id={`product-price-${index}`}>Php {product.price}</td>
                  <td id="table-header-quantity">{product.quantity}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <h3 id="total-amount">Total Amount: Php {totalAmount}</h3>
        </div>
        <CheckOut userDetails={userDetails} orderedProduct={selectedProducts} totalAmount={totalAmount}/>
      </div>
  );
}

export default ReceiptPage;
