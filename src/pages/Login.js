import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'; 
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login () {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(true);


    function authenticate(e) {

        e.preventDefault();
		fetch('https://cpstn2-ecommerceapi-lago.onrender.com/users/login',{

		method: 'POST',
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({

			email: email,
			password: password

		})
	})
	.then(res => res.json())
	.then(data => {

		// console.log(data.access);

		if(!data.access){

			Swal.fire({
                title: "Authentication failed",
                icon: "error",
                text: "Check your login details and try again."
            });
			
		} else {

			localStorage.setItem('token', data.access);
			retrieveUserDetails(data.access);

			setUser({
				access: localStorage.getItem('token')
			})

			Swal.fire({
        	    title: "Login Successful",
        	    icon: "success",
        	    text: "WELCOME BACK!"
        	});
		}
	})

	setEmail('');
	setPassword('');


    }


    const retrieveUserDetails = (token) => {
 
        fetch('https://cpstn2-ecommerceapi-lago.onrender.com/users/details', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });

        })

    };

    useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    return (
    	(user.id !== null) ?

			<Navigate to="/" />

			:
	    	
			<Container className="mt-5">
		      <Row className="justify-content-center">
		        <Col md={6}>
		          <Form onSubmit={authenticate}>
		            <h1 className="text-center mb-4">Login</h1>
		            <Form.Group controlId="userEmail">
		              <Form.Label>Email address</Form.Label>
		              <Form.Control
		                type="email"
		                placeholder="Enter email"
		                value={email}
		                onChange={(e) => setEmail(e.target.value)}
		                required
		              />
		            </Form.Group>

		            <Form.Group controlId="password" className="pb-3">
		              <Form.Label>Password</Form.Label>
		              <Form.Control
		                type="password"
		                placeholder="Password"
		                value={password}
		                onChange={(e) => setPassword(e.target.value)}
		                required
		              />
		            </Form.Group>

		            <p>Not yet registered? <a href="/register">Click Here</a></p>

		            {isActive ? (
		              <Button variant="primary" type="submit" id="submitBtn">
		                Submit
		              </Button>
		            ) : (
		              <Button variant="danger" type="submit" id="submitBtn" disabled>
		                Submit
		              </Button>
		            )}
		          </Form>
		        </Col>
		      </Row>
		    </Container>
    )
}