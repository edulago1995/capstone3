import {useState,useEffect, useContext} from 'react';
import {Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate,Navigate,Link } from 'react-router-dom';
import "../App.css";

export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})
    console.log(details);
    // console.log(user);

    useEffect(()=>{

        fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (typeof data._id !== "undefined") {

				setDetails(data);

			}
        });

    },[])

	return (
		  (user.id === null) ?
		  <Navigate to="/products" />
		  :
		  <>
		    <Row>
		      <Col className="p-5">
		        <div className="text-center">
		          <h1 className="mt-3 display-4" >{`${details.firstName} ${details.lastName}`}</h1>
		        </div>
		        <hr className="my-4" />
		        <div className="text-center">
		          <h4>Contacts</h4>
		          <ul className="list-unstyled">
		            <li><i className="bi bi-envelope"></i> Email: {details.email}</li>
		            <li><i className="bi bi-phone"></i> Mobile No: {details.mobileNo}</li>
		          </ul>
		        </div>
		      </Col>
		    </Row>
			<div className="text-center">
			      <Link className="btn btn-primary me-3" to={`/updateprofile`}>Update Profile</Link>
			      <Link className="btn btn-primary" to={`/reset-password`}>Reset Password</Link>
	    	</div>
		  </>
	)

}