import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import CustomNumberInput from '../components/CustomNumberInput';

export default function ProductView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1); // Initialize quantity with a default value of 1

  const order = () => {
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/users/orderProduct`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity // Use the user-provided quantity here
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data.message);

      if (data.message === 'Order Product Successfully.') {
        Swal.fire({
          title: "added to cart",
          icon: 'success',
          text: "You have successfully added this product to your cart."
        });

        navigate("/products");
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        });
      }
    });
  };

  useEffect(() => {
    console.log(productId);

    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/products/${productId}`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
    });
  }, [productId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              {user.id !== null ? (
                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                  <div className="mb-2 mt-2">
                    <CustomNumberInput quantity={quantity} setQuantity={setQuantity} />
                  </div>
                  <div>
                    <Button variant="primary" block onClick={order}>
                      Add to Cart
                    </Button>
                  </div>
                </div>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Log in to Order
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
