import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

export default function Products() {

	const { user } = useContext(UserContext);

	const [products, setProdcuts] = useState([]);


	const fetchData = () => {
		fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/products/all`)
		.then(res => res.json())
		.then(data => {
		    
		    console.log(data);

		    setProdcuts(data);

		});
	}

    useEffect(() => {

		fetchData()

    }, []);

	return(
		<>
            {
            	(user.isAdmin === true) ?
            		<AdminView productsData={products} fetchData={fetchData} />

            		:

            		<UserView productsData={products} />

        	}
        </>
	)
}











