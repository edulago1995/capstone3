import React, { useState, useEffect } from 'react';
import CancelOrder from '../components/CancelOrder';
import "../App.css";

function MyPurchase({ ordersData, fetchData }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/myorders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        // console.log("Data from API:", data); // Check the data received from the API
        if (Array.isArray(data) && data.length > 0) {
          setOrders(data);
        }
      });
  }, [ordersData, fetchData]);

  // console.log("Final Orders:", orders); // Debugging: Check the orders just before rendering

  return (
  <div>
    <h1 id="receipt-header" className="text-center my-3">
      My Purchase
    </h1>
    {orders.map((order, orderIndex) => (
      <div key={orderIndex} className="receipt-card">
        <td
          className={
            order.isActive && order.isPending
              ? 'text-success text-center'
              : order.isActive && !order.isPending
              ? 'text-success text-center'
              : !order.isActive && order.isPending
              ? 'text-danger text-center'
              : 'text-success text-center'
          }
        >
          {order.isActive && order.isPending ? (
            <strong>pending</strong>
          ) : order.isActive && !order.isPending ? (
            <strong>shipped</strong>
          ) : !order.isActive && order.isPending ? (
            <strong>Canceled</strong>
          ) : (
            <strong>DELIVERED</strong>
          )}
        </td>
        <div className="receipt-content">
          <table id="receipt-table">
            <thead>
              <tr>
                <th id="table-header-name">Product Name</th>
                <th id="table-header-quantity" className="text-right">
                  Quantity
                </th>
                <th id="table-header-price" className="text-right">
                  Price
                </th>
              </tr>
            </thead>
            <tbody>
              {order.orderedProducts.map((productSet, productIndex) => (
                <React.Fragment key={productIndex}>
                  {productSet.products.map((product, innerProductIndex) => (
                    <tr key={innerProductIndex}>
                      <td id="table-header-name">{product.name}</td>
                      <td id="table-header-quantity" className="text-right">
                        {product.quantity}
                      </td>
                      <td id="table-header-price" className="text-right">
                        Php {product.price}
                      </td>
                    </tr>
                  ))}
                </React.Fragment>
              ))}
            </tbody>
          </table>
          <div className="order-info">
            {order.orderedProducts.map((productSet, productSetIndex) => (
              <div key={productSetIndex} className="product-set">
                <h3 id="total-amount">Total Amount: Php {productSet.totalAmount}</h3>
                <p>
                  Purchased On: {new Date(productSet.purchasedOn).toLocaleString('en-US', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                  })}
                </p>
              </div>
            ))}
            <CancelOrder user={order._id} isPending={order.isPending} isActive={order.isActive}/>
          </div>
        </div>
      </div>
    ))}
  </div>
);
}

export default MyPurchase;







// ONE DATA PALANG
// import React, { useState, useEffect } from 'react';
// import "../App.css";

// function MyPurchase() {
//   const [details, setDetails] = useState({});
//   const [data, setData] = useState([]);

//   useEffect(() => {
//   fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/myorders`, {
//     headers: {
//       Authorization: `Bearer ${localStorage.getItem('token')}`
//     }
//   })
//     .then(res => res.json())
//     .then(data => {
//       console.log("Data from API:", data); // Check the data received from the API
//       if (Array.isArray(data) && data.length > 0) {
//         setDetails(data[0]);
//         setData(data[0].orderedProducts);
//         console.log("Details:", details); // Check the details state
//         console.log("Data:", data); // Check the data state
//       }
//     });
// }, []);

// console.log("Final Data:", data); // Check the data just before returning


//   return (
//     <div className="receipt-card">
//       <h1 id="receipt-header">My Purchase</h1>
//       <div className="receipt-content">
//         <table id="receipt-table">
//           <thead>
//             <tr>
//               <th id="table-header-name">Product Name</th>
//               <th id="table-header-price">Price</th>
//               <th id="table-header-quantity">Quantity</th>
//             </tr>
//           </thead>
//           <tbody>
//             {data[0] && data[0].products && data[0].products.map((product, index) => (
// 			  <tr key={index}>
// 			    <td>{product.name}</td>
// 			    <td id={`product-price-${index}`}>Php {product.price}</td>
// 			    <td id={`product-quantity-${index}`}>{product.quantity}</td>
// 			  </tr>
// 			))}
//           </tbody>
//         </table>
//         {/* Display the totalAmount from the details object */}
//         <h3 id="total-amount">Total Amount: Php {data[0] && data[0].totalAmount}</h3>
//       </div>
//     </div>
//   );
// }

// export default MyPurchase;
