import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';

import AllOrders from '../components/AllOrders';

export default function Orders() {

	const { user } = useContext(UserContext);

	const [orders, setProdcuts] = useState([]);


		const fetchData = () => {
			fetch(`https://cpstn2-ecommerceapi-lago.onrender.com/orders/allorders`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
			    
			    console.log(data);

			    setProdcuts(data);

			});
		}

	    useEffect(() => {

			fetchData()

	    }, []);


	return(
		<>
            {
            	(user.isAdmin === true) ?
            		<AllOrders ordersData={orders} fetchData={fetchData} />

            		:

            		<AllOrders ordersData={orders} />

        	}
        </>
	)
}
